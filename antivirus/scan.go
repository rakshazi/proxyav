// ClamAV integration with ProxyAV
package antivirus

import (
	"bytes"
	"log"
	"os/exec"
	"strings"
)

var env []string

//Scan - run clam(d)scan
func Scan(clambinary string, args ...string) bool {
	var output bytes.Buffer
	cmd := exec.Command(clambinary, args...)
	cmd.Env = env
	cmd.Stdout = &output
	cmd.Stderr = &output
	err := cmd.Run()
	if err != nil {
		log.Println(err)
		return false
	}
	text := output.String()
	log.Println("ClamAV output:", text)
	if strings.Contains(text, "Total errors") {
		log.Println(clambinary, "error")
		return false
	}
	if strings.Contains(text, "Infected files: 0") {
		return true
	}
	return false
}

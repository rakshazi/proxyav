package antivirus

import (
	"fmt"
	"os"
	"testing"
)

func TestScan(t *testing.T) {
	env = []string{"GO_TEST_PROCESS=1"}
	tables := []struct {
		Name     string
		Function string
		Result   bool
	}{
		{"not-infected", "TestScanMockNotInfected", true},
		{"infected", "TestScanMockInfected", false},
		{"cmd-failed", "TestScanMockFailed", false},
		{"unknown", "TestScanUnknownFailed", false},
	}
	for _, table := range tables {
		t.Run(table.Name, func(t *testing.T) {
			result := Scan(os.Args[0], "-test.run="+table.Function)
			if table.Result != result {
				t.Fail()
			}
		})
	}
}

func TestScanMockNotInfected(t *testing.T) {
	if os.Getenv("GO_TEST_PROCESS") != "1" {
		return
	}

	fmt.Fprintf(os.Stdout, "GO TEST STUB: Infected files: 0")
	os.Exit(0)
}

func TestScanMockInfected(t *testing.T) {
	if os.Getenv("GO_TEST_PROCESS") != "1" {
		return
	}

	fmt.Fprintf(os.Stdout, "GO TEST STUB: Total errors: 5")
	os.Exit(0)
}

func TestScanMockUnknownFailed(t *testing.T) {
	if os.Getenv("GO_TEST_PROCESS") != "1" {
		return
	}

	fmt.Fprintf(os.Stdout, "GO TEST STUB: something is wrong")
	os.Exit(0)
}

func TestScanMockCmdFailed(t *testing.T) {
	if os.Getenv("GO_TEST_PROCESS") != "1" {
		return
	}
	fmt.Fprintf(os.Stdout, "GO TEST STUB: clamd not found")
	os.Exit(2)
}

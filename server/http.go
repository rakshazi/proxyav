// HTTP webserver with reverse proxy mode and ClamAV integration
package server

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"gitlab.com/rakshazi/proxyav/antivirus"
)

// ErrorResponse will be returned if file contains a virus
type ErrorResponse struct {
	Code        int
	ContentType string
	Body        string
}

var server *http.Server
var response *ErrorResponse
var remote string
var avBinary string

// Run webserver
func Run(port string, destination string, rCode int, rContentType string, rBody string, clamBinary string) {
	remote = destination
	avBinary = clamBinary
	response = &ErrorResponse{
		Code:        rCode,
		ContentType: rContentType,
		Body:        rBody,
	}
	server = &http.Server{
		Addr:         ":" + port,
		ReadTimeout:  60 * time.Second,
		WriteTimeout: 60 * time.Second,
	}
	http.HandleFunc("/", Handler)

	log.Fatal(server.ListenAndServe())
}

// Handler - HTTP handler
func Handler(w http.ResponseWriter, r *http.Request) {
	if r.Body == nil {
		forward(w, r, nil)
		return
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	r.Body = ioutil.NopCloser(bytes.NewReader(body))
	r.ParseMultipartForm(32 << 20)
	valid := process(r)
	clientIP := getClientIP(r)
	if valid {
		log.Println(r.Method, r.URL.Path, clientIP, "valid")
		forward(w, r, body)
		return
	} else {
		log.Println(r.Method, r.URL.Path, clientIP, "invalid")
		w.Header().Set("Content-Type", response.ContentType+"; charset=utf-8")
		w.Header().Set("X-Content-Type-Options", "nosniff")
		w.WriteHeader(response.Code)
		fmt.Fprintln(w, response.Body)
	}
}

// Check client request, if it's valid - return true
func process(r *http.Request) bool {
	if r.MultipartForm == nil || len(r.MultipartForm.File) == 0 {
		return true
	}
	for filename := range r.MultipartForm.File {
		file, handler, err := r.FormFile(filename)
		path := "/tmp/" + handler.Filename
		if err != nil {
			log.Println(err)
			return false
		}
		f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			log.Println(err)
			return false
		}
		_, err = io.Copy(f, file)
		if err != nil {
			log.Println(err)
			return false
		}
		response := antivirus.Scan(avBinary, "-i", "--fdpass", path)
		file.Close()
		os.Remove(path)
		if !response {
			return false
		}
	}

	return true
}

// Forward client's request to destination AND return destination's response to client
func forward(w http.ResponseWriter, r *http.Request, rBody []byte) http.ResponseWriter {
	target, _ := url.Parse(remote)
	targetQuery := target.RawQuery
	clone, err := http.NewRequest(r.Method, remote, bytes.NewReader(rBody))
	copyHeader(clone.Header, r.Header)
	if err != nil {
		log.Println("forward clone", err)
	}
	clone.URL.Path = strings.TrimRight(singleJoiningSlash(target.Path, r.URL.Path), "/")
	if targetQuery == "" || r.URL.RawQuery == "" {
		clone.URL.RawQuery = targetQuery + r.URL.RawQuery
	} else {
		clone.URL.RawQuery = targetQuery + "&" + r.URL.RawQuery
	}
	if clientIP, _, err := net.SplitHostPort(r.RemoteAddr); err == nil {
		// If we aren't the first proxy retain prior
		// X-Forwarded-For information as a comma+space
		// separated list and fold multiple headers into one.
		if prior, ok := r.Header["X-Forwarded-For"]; ok {
			clientIP = strings.Join(prior, ", ") + ", " + clientIP
		}
		clone.Header.Set("X-Forwarded-For", clientIP)
	}
	resp, err := http.DefaultTransport.RoundTrip(clone)
	if err != nil {
		log.Println("forward error", err)
		return w
	}
	defer resp.Body.Close()
	copyHeader(w.Header(), resp.Header)
	w.WriteHeader(resp.StatusCode)
	io.Copy(w, resp.Body)

	return w
}

// Copy headers from client request to new proxied request
func copyHeader(target, source http.Header) {
	for k, vv := range source {
		for _, v := range vv {
			target.Add(k, v)
		}
	}
}

// Remove duplicate slashes in request path,
// eg: /destination/endpoint + /sent-by-client = /destination/endpoint/sent-by-client
func singleJoiningSlash(a, b string) string {
	aslash := strings.HasSuffix(a, "/")
	bslash := strings.HasPrefix(b, "/")
	switch {
	case aslash && bslash:
		return a + b[1:]
	case !aslash && !bslash:
		return a + "/" + b
	}
	return a + b
}

// Get Client IP fron X-Real-Ip or X-Forwarded-For or RemoteAddr
func getClientIP(r *http.Request) string {
	if r.Header.Get("X-Real-Ip") != "" {
		return r.Header.Get("X-Real-Ip")
	}
	ips := strings.Split(r.Header.Get("X-Forwarded-For"), ", ")
	if len(ips) > 0 && ips[0] != "" {
		return ips[0]
	}
	return r.RemoteAddr
}
